const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js');
mix.sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/pages/welcome.js', 'public/js');
mix.sass('resources/sass/pages/welcome.scss', 'public/css');

mix.js('resources/js/pages/details.js', 'public/js');
mix.sass('resources/sass/pages/details.scss', 'public/css');
