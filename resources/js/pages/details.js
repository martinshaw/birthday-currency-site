import VueApexCharts from 'vue-apexcharts'

Vue.component('apexchart', VueApexCharts);

const app = new Vue({
    el: '#app',
});