// Loads JS content for Bootstrap UI Framework
require('bootstrap');

// Manually pre-load background-image
if(document.images){
    var bgImage = new Image(422,156);
    bgImage.src = "/images/lighted-candles-on-cupcakes-40183.jpg";
}

// Loads library used for API requests, also sets defaults for POST requests
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

window.Vue = require('vue');

// Loads all available Vue.js components from components directory
const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Using Bootstrap media query breakpoints to change Vue.js behaviour
import VueMq from 'vue-mq';
Vue.use(VueMq, {
    breakpoints: {
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
    }
});