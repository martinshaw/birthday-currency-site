@extends('layouts.app')

@section('asset_preloading')
    @parent
    <link rel="preload" href="{{ asset('/css/welcome.css') }}" as="style">
    <link rel="preload" href="{{ asset('/js/welcome.js') }}" as="script">
@endsection
@section('styles')
    @parent
    <link href="{{ asset('/css/welcome.css') }}" rel="stylesheet">
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('/js/welcome.js') }}" defer></script>
@endsection


@section('content')
    <div class="welcome_form_container">
        <div class="position-relative overflow-hidden text-center">
            <div class="col-sm-10 col-md-9 col-lg-7 p-3 p-md-5 p-lg-6 mx-auto my-5">
                @if ($errors->any())
                    <div class="rounded-lg text-left mx-auto bg-danger text-white">
                        <ul class="py-2 pl-5 pr-4">
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <welcome-form
                    :currency-codes="{{ json_encode($symbols ?? (object)[]) }}"
                    :form-disabled="{{ ($formDisabled ?? false) ? 'true' : 'false' }}"
                ></welcome-form>
            </div>
        </div>
    </div>
    <div class="welcome_submissions_preview_container">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-12 p-4 h-100">
                    <submission-browser :year-from="{{ now()->year -10 }}" :year-to="{{ now()->year }}" :per-row="$mq | mq({lg: 2, xl: 4})"></submission-browser>
                </div>
            </div>
        </div>
    </div>
    
@endsection