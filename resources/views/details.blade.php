@extends('layouts.app')

@section('asset_preloading')
    @parent
    <link rel="preload" href="{{ asset('/css/details.css') }}" as="style">
    <link rel="preload" href="{{ asset('/js/details.js') }}" as="script">
@endsection
@section('styles')
    @parent
    <link href="{{ asset('/css/details.css') }}" rel="stylesheet">
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('/js/details.js') }}" defer></script>
@endsection


@section('content')
    <div class="details_line_graph_container">
        <div class="position-relative overflow-hidden container text-center h-100">
            <div class="py-5 h-100">
                <details-line-graph submission="{{ $submissionUuid }}"></details-line-graph>
            </div>
        </div>
    </div>
    <div class="details_submissions_preview_container">
        <div class="container h-100">
            <div class="row">
                <div class="col-md-12 pb-2">
                    <submission-card submission="{{ $submissionUuid }}" :is-latest="true"></submission-card>
                </div>
            </div>
            <div class="row h-100">
                <div class="col-md-12 p-4 h-100">
                    <submission-browser :year-from="{{ now()->year -10 }}" :year-to="{{ now()->year }}" :per-row="$mq | mq({lg: 2, xl: 4})"></submission-browser>
                </div>
            </div>
        </div>
    </div>
@endsection