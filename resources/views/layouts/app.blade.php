<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="csrf-token" content="{{csrf_token()}}"/>

        <title>Birthday Currency Site - @yield('title', 'Welcome')</title>

        <!-- Add DNS Prefetching -->
        <link rel="dns-prefetch" href="{{ str_replace(['https://', 'http://'], '//', config('app.url')) }}">

        <!-- Asset Preloading -->
        @section('asset_preloading')
        <link rel="preload" href="{{ asset('/images/lighted-candles-on-cupcakes-40183.jpg') }}" as="image">
        <link rel="preload" href="{{ asset('/images/dept_logo.svg') }}" as="image">
        <link rel="preload" href="{{ asset('/css/site.css') }}" as="style">
        <link rel="preload" href="{{ asset('/js/site.js') }}" as="script">
        @show

        <!-- Styles -->
        @section('styles')
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        @show
    </head>
    <body>

        <nav class="site-header sticky-top shadow-sm py-1 bg-white">
            <div class="container d-flex flex-row justify-content-between">
                @if (Route::current()->getName() === 'submission.index')
                    <a class="py-2 d-none d-sm-inline-block">
                        Welcome to the {{ config('app.name') }}
                    </a>
                @else
                    <a class="py-2 d-inline-block" href="/">
                        &larr;&nbsp;Return to homepage
                    </a>
                @endif

                <a class="py-2 d-inline-block text-right">
                    <img height="20px" src="/images/dept_logo.svg" alt="Dept Agency Logo">&nbsp;🎂&nbsp;&nbsp;💰
                </a>
            </div>
        </nav>

        <div id="app">
            <main class="">@yield('content')</main>
        </div>

        <footer class="container">
            <div class="row text-muted py-md-5">
                <div class="col-md-5 py-2 py-md-0">&copy; 2019, Dept Agency (website concept), Martin Shaw (implementation).</div>
                <div class="col-md-7 py-2 py-md-0">
                    This website was created as part of a job application for Dept Agency. It is built upon Laravel PHP framework,
                    with Laravel Reponder & Query Builder packages, using the Bootstrap UI & Vue.js frameworks.
                    </div>
            </div>
        </footer>

        <!-- Scripts -->
        @section('scripts')
        <script src="{{ asset('/js/app.js') }}" defer></script>
        @show

    </body>
</html>
