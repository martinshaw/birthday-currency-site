<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'unauthenticated' => 'You are not authenticated for this request.',
    'unauthorized' => 'You are not authorized for this request.',
    'page_not_found' => 'The requested page does not exist.',
    'relation_not_found' => 'The requested relation does not exist.',
    'validation_failed' => 'The given data failed to pass validation.',

    'model_not_found' => 'The requested :model_type cannot be found',
    'fixer_currency_service_unavailable' => 'An error occurred when attempting to get currency information. Please try again later!',

];