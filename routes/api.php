<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/submissions', 'API\SubmissionController@index');
Route::get('/submissions/{submissionUuid}', 'API\SubmissionController@show')
    ->where('submissionUuid', '[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}');
Route::get('/submissions/{submissionUuid}/history', 'API\SubmissionHistoryController@index')
    ->where('submissionUuid', '[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}');

