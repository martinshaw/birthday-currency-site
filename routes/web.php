<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$uuid4RegexCriteria = '[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}';

Route::get('/', 'SubmissionController@index')->name('submission.index');
Route::get('/{uuid}', 'SubmissionController@show')->name('submission.show')->where('uuid', $uuid4RegexCriteria);
Route::post('/', 'SubmissionController@store')->name('submission.store');
