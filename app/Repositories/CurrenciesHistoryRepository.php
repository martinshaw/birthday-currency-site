<?php
namespace App\Repositories;

use Carbon\Carbon;
use App\ExternalServices\FixerCurrencyService;

class CurrenciesHistoryRepository
{
    protected $fixerService;

    public function __construct()
    {
        $this->fixerService = app(FixerCurrencyService::class);
    }

    public function getHistoryByLatestDate(Carbon $latestDate, $currencies = ['USD'], $invertSort = true, $depth = 5)
    {
        $history = [];
        for ($i = 0; $i < $depth; $i++) {
            // Often, when the Fixer API doesn't have the requested depth of historical data, the API will return a 'failed' response.
            // Here we will assume that, on exception, if the history array has at least one piece of historical data,
            // then the eventual exception must be because of insufficient available data. On this occasion, we will
            // only return the available depth.
            try {
                $history[] = (array) $this->fixerService->getHistorical($latestDate, $currencies);
            } catch (\Exception $exception) {
                if (empty($history)) {
                    throw $exception;
                }
                continue;
            }

            $latestDate = $latestDate->subYear();
        }

        return ($invertSort) ? array_reverse($history) : $history;
    }
}