<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Submission;
use App\Enums\Months;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\SubmissionRequest;
use App\ExternalServices\FixerCurrencyService;
use App\Repositories\CurrenciesHistoryRepository;
use App\Exceptions\FixerCurrencyResponseException;

class SubmissionController extends Controller
{
    /**
     * Displays Welcome view page with currency symbol options populated
     *
     * @param FixerCurrencyService $fixer
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(FixerCurrencyService $fixer)
    {
        try {
            $symbols = $fixer->getSymbols();
        } catch (\Exception $exception) {
            return view('welcome', ['formDisabled' => true])
                ->withErrors(['fixer' => 'Unfortunately, we were unable to access the Fixer currency service. Please try again later!']);
        }

        return view('welcome', compact('symbols'));
    }

    /**
     * Stores new Submission from Welcome view page then redirects to Details view page
     *
     * @param SubmissionRequest $request
     * @param FixerCurrencyService $fixer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SubmissionRequest $request, FixerCurrencyService $fixer)
    {
        try {
            $latestBirthday = $this->buildLatestBirthdayFromStoreRequest($request);
        } catch (\Exception $exception) {
            Log::error('An error occurred when attempting to format provided date: '.$exception->getMessage());
            return back()->withErrors(['submission' => 'We were unable to format a provided date. Please try again later']);
        }
        
        try {
            $latestRate = $fixer->getHistorical($latestBirthday, [$request->get('currency')])->rates->{$request->get('currency')};
        } catch (FixerCurrencyResponseException $exception) {
            return back()->withErrors(['submission' => $exception->getMessage()]);
        }

        try {
            $resource = Submission::create([
                'latest_birthday' => $latestBirthday,
                'latest_birthday_day' => $latestBirthday->day,
                'latest_birthday_month' => $latestBirthday->englishMonth,
                'latest_birthday_year' => $latestBirthday->year,
                'currency_code' => $request->get('currency'),
                'latest_rate' => $latestRate,
            ]);
        } catch (\Exception $exception) {
            Log::critical('An error occurred when attempting to create a new Submission resource: '.$exception->getMessage());
            return back()->withErrors(['submission' => 'We were unable to process the request. Please try again later']);
        }
        
        return redirect()->route('submission.show', ['uuid' => $resource->uuid]);
    }

    /**
     * Show details page for a singular submission, including its history and other submissions
     *
     * @param CurrenciesHistoryRepository $repo
     * @param string $uuid
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show(CurrenciesHistoryRepository $repo, string $submissionUuid)
    {
        $resource = Submission::where('uuid', $submissionUuid)->first();
        if (empty($resource)) {
            return redirect()->route('submission.index')
                ->withErrors([
                    'submission' => 'We were unable to find a previous request with the provided information. Please enter the details again.'
                ]);
        }

        return view('details', compact('submissionUuid', 'resource'));
    }

    /**
     * Format date from day/month request params into a Carbon object of the latest birthday
     *
     * @param SubmissionRequest $request
     * @return Carbon
     */
    public function buildLatestBirthdayFromStoreRequest(SubmissionRequest $request)
    {
        $currentYear = Carbon::now()->year;
        $monthNumber = array_flip(Months::all())[$request->get('birthdateMonth')] +1;

        $latestBirthday = Carbon::createMidnightDate($currentYear, $monthNumber, $request->get('birthdateDay'), config('app.timezone'));
        if ($latestBirthday->diffInDays(Carbon::now(), false) <= 0) {
            $latestBirthday = Carbon::createMidnightDate($currentYear -1, $monthNumber, $request->get('birthdateDay'), config('app.timezone'));
        }
        return $latestBirthday;
    }
}