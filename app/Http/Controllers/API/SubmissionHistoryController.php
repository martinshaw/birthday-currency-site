<?php
namespace App\Http\Controllers\API;

use App\Submission;
use Flugg\Responder\Facades\Responder;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\CurrenciesHistoryRepository;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;


class SubmissionHistoryController extends \App\Http\Controllers\Controller
{
    public function index(CurrenciesHistoryRepository $repo, string $submissionUuid)
    {
        $resource = Submission::where('uuid', $submissionUuid)->firstOrFail();
        $history = collect($repo->getHistoryByLatestDate($resource->latest_birthday, [$resource->currency_code], true, 10));
        
        return responder()->success($history)->respond();
    }
}