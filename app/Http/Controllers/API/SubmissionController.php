<?php
namespace App\Http\Controllers\API;

use App\Submission;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Redirect;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class SubmissionController extends \App\Http\Controllers\Controller
{
    public function index()
    {
        $paginator = QueryBuilder::for(Submission::class)
            ->allowedFilters(Submission::$columns)
            ->defaultSort('id')
            ->allowedSorts(Submission::$columns)
            ->paginate();
        $adapter = new IlluminatePaginatorAdapter($paginator);

        return responder()->success($paginator->getCollection())->paginator($adapter)->respond();
    }

    public function show($submissionUuid)
    {
        $submission = QueryBuilder::for(Submission::class)
            ->allowedFilters(Submission::$columns)
            ->where('uuid', $submissionUuid)->firstOrFail();

        return responder()->success($submission)->respond();
    }
}