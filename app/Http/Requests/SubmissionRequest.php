<?php

namespace App\Http\Requests;

use App\Enums\Months;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\ExternalServices\FixerCurrencyService;

class SubmissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currencyCodes = array_keys((array)app(FixerCurrencyService::class)->getSymbols());
        return [
            'birthdateDay' => ['required', 'numeric', 'min:1', 'max:31'],
            'birthdateMonth' => ['required', 'string', Rule::in(Months::all())],
            'currency' => ['required', 'string', Rule::in($currencyCodes)]
        ];
    }

    /**
     * Provide custom validation messages where needed
     *
     * @return array
     */
    public function messages()
    {
        return [
            'currency.in' => 'The selected currency is not currently supported'
        ];
    }
}
