<?php
namespace App\Enums;

class Months
{
    const January = 'January';
    const February = 'February';
    const March = 'March';
    const April = 'April';
    const May = 'May';
    const June = 'June';
    const July = 'July';
    const August = 'August';
    const September = 'September';
    const October = 'October';
    const November = 'November';
    const December = 'December';

    public static function all()
    {
        return [
            static::January,
            static::February,
            static::March,
            static::April,
            static::May,
            static::June,
            static::July,
            static::August,
            static::September,
            static::October,
            static::November,
            static::December,
        ];
    }
}