<?php

namespace App;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    public static $columns = [
        'id',
        'uuid',
        'latest_birthday',
        'latest_birthday_day',
        'latest_birthday_month',
        'latest_birthday_year',
        'currency_code',
        'latest_rate',
        'created_at',
        'updated_at'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::uuid4();
        });
    }

    protected $dates = ['latest_birthday', 'created_at, updated_at'];
    protected $fillable = [
        'latest_birthday',
        'latest_birthday_day',
        'latest_birthday_month',
        'latest_birthday_year',
        'currency_code',
        'latest_rate',
    ];

    public function scopeByYear($query, int $year)
    {
        return $query->where('latest_birthday_year', $year);
    }

    public function scopeByMonth($query, string $month)
    {
        return $query->where('latest_birthday_month', $month);
    }
}
