<?php

namespace App\Exceptions\ConvertableHttpExceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException as EloquentModelNotFoundException;
use Illuminate\Support\Str;

class ModelNotFoundException extends \Flugg\Responder\Exceptions\Http\HttpException
{
    /**
     * @var EloquentModelNotFoundException
     */
    protected $originalException;

    /**
     * The HTTP status code.
     *
     * @var int
     */
    protected $status = 404;

    /**
     * The error code.
     *
     * @var string|null
     */
    protected $errorCode = 'model_not_found';

    /**
     * The error message.
     *
     * @var string|null
     */
    protected $message;

    /**
     * ModelNotFoundException constructor.
     * @param EloquentModelNotFoundException $originalException
     */
    public function __construct(EloquentModelNotFoundException $originalException)
    {
        $this->originalException = $originalException;
        $this->message = $this->buildMessage();

        parent::__construct($this->message);
    }

    /**
     * @return string
     */
    public function buildMessage ()
    {
        $type = class_basename($this->originalException->getModel());
        return __('errors.model_not_found', [
            'model_type' => count($this->originalException->getIds()) > 1 ? Str::plural($type) : $type
        ]);
    }
}