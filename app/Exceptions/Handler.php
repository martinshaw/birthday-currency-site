<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;

use Flugg\Responder\Exceptions\ConvertsExceptions;
use Flugg\Responder\Exceptions\Http\PageNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use App\Exceptions\ConvertableHttpExceptions\ModelNotFoundException;
use Flugg\Responder\Exceptions\Http\HttpException as ResponderHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException as EloquentModelNotFoundException;

class Handler extends ExceptionHandler
{
    use ConvertsExceptions;
    
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
		ModelNotFoundException::class,
		HttpException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    
    public function __construct() {
        // This will stop select default exceptions from being converted by
        // the default converter so I can use my custom conversion instead
        $this->dontConvert = [
            EloquentModelNotFoundException::class,
            ValidationException::class
        ];
    }

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $this->convertDefaultException($exception);

        $this->convert($exception, [
            EloquentModelNotFoundException::class => function ($exception) {
                throw new ModelNotFoundException($exception);
            },
        ]);
        
        if ($exception instanceof ResponderHttpException and $request->wantsJson()) {
            return $this->renderResponse($exception);
        }

        return parent::render($request, $exception);
    }
}
