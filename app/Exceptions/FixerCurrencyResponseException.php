<?php
namespace App\Exceptions;

use Exception;
use GuzzleHttp\Psr7\Response;

class FixerCurrencyResponseException extends \Flugg\Responder\Exceptions\Http\HttpException
{
    /**
     * The HTTP status code.
     *
     * @var int
     */
    protected $status = 404;

    /**
     * The error code.
     *
     * @var string|null
     */
    protected $errorCode = 'fixer_currency_service_unavailable';

    /**
     * The error message.
     *
     * @var string|null
     */
    protected $message = 'An error occurred when attempting to get currency information. Please try again later!';
}
