<?php
namespace App\ExternalServices;

use Carbon\Carbon;
use GuzzleHttp\Client;

class FixerCurrencyService
{
    use FixerCurrencyCachingTrait;

    protected $client;
    protected $key;

    public function __construct(Client $client, string $key)
    {
        $this->client = $client;
        $this->key = $key;
    }

    /**
     * API request method for /latest endpoint
     *
     * @param array $symbols
     * @return object
     */
    public function getLatest($symbols = ['USD'])
    {
        return $this->cachingRequest('GET', 'latest', [
            'query' => [
                'access_key' => $this->key,
                'symbols' => implode(',', $symbols),
            ]
        ], false);
    }

    /**
     * API request method for /{date} historical endpoint
     *
     * @param Carbon $date
     * @param array $symbols
     * @return object
     */
    public function getHistorical(Carbon $date, $symbols = ['USD'])
    {
        return $this->cachingRequest('GET', $date->toDateString(), [
            'query' => [
                'access_key' => $this->key,
                'symbols' => implode(',', $symbols),
            ]
        ]);
    }

    /**
     * API request method for /symbols endpoint
     *
     * @return object
     */
    public function getSymbols()
    {
        return $this->cachingRequest('GET', 'symbols', [
            'query' => [
                'access_key' => $this->key,
            ]
        ])->symbols;
    }
    
    // Additional endpoints will be implemented as needed
}