<?php
namespace App\ExternalServices;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\FixerCurrencyResponseException;

trait FixerCurrencyCachingTrait
{
    /**
     * Cache handler for Fixer Currency API requests
     *
     * @param string $method
     * @param string $uri
     * @param array $options
     * @param integer $cacheDuration In Days
     * @return object|array|\stdClass
     */
    protected function cachingRequest($method, $uri = '', array $options = [], $cacheDuration = 1)
    {
        $tagName = $this->generateCachingTagName($method, $uri, $options);
        if (cache()->has($tagName) and empty($cacheDuration) === false) {
            return json_decode(cache()->get($tagName));
        }

        try {
            $response = $this->client->request($method, $uri, $options);
            $handledResponse = json_decode($response->getBody()->getContents());
        } catch (\Exception $exception) {
            Log::error("A Guzzle HTTP error occurred when contacting Fixer API: {$exception->getMessage()}");
            throw new FixerCurrencyResponseException;
        }
        if ($handledResponse->success === false) {
            Log::error("An error occurred when contacting Fixer API: {$handledResponse->error->code} {$handledResponse->error->type}: {$handledResponse->error->info}");
            throw new FixerCurrencyResponseException;
        }
        
        if (empty($cacheDuration) === false) {
            $cachedResponse = cache()->put($tagName, json_encode($handledResponse), (60 * 60 * 24) * $cacheDuration);
        }
        return $handledResponse;
    }

    /**
     * Generates a consistent tag for these particular request contents
     *
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return string
     */
    protected function generateCachingTagName($method, $uri, array $options)
    {
        $options = json_encode($options);
        return base64_encode("{$method}::{$uri}::{$options}");
    }
}