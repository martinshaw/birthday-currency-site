<?php
namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use App\ExternalServices\FixerCurrencyService;
use Illuminate\Support\ServiceProvider;

class FixerCurrencyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FixerCurrencyService::class, function () {
            return new FixerCurrencyService($this->generateClient(), config('services.fixer.key'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    protected function generateClient()
    {
        return new Client([
            'base_uri' => config('services.fixer.base_url'),
        ]);
    }
}
