# Birthday Currency Site
**This project is a trial test created to demonstrate my knowledge of Laravel during a job application**


### Requirements
The following software is required to run the site on your server:

*  Apache 2
*  PHP 7.2 (with BCMath, Ctype, JSON, Mbstring, OpenSSL, PDO, Tokenizer, and XML extensions)
*  Redis
*  MySQL


### Installation Step

Run the following commands to compile the production assets

```sh
composer install
cp .env.example .env
npm install
npm run production
```

Go to http://fixer.io and create a new API account. Copy/paste the provided API key into the `.env` file's environmental variable `FIXER_KEY` value.

### Original Brief

The following is the brief provided by email from the enquiring company:
  
> **Task**
>
> Using the following service [http://fixer.io/] you are to create a simple web app which allows a user to find out what the exchange rate (for a currency of your choice) was on their previous birthday.
> 
> You are to store all submissions to the site and display them on the webpage. One caveate to this is if the same birthday occurs twice then this should be indicated with the number of times that birthday has occurred not duplicate entries.
> 
> The results should be shown in date order with the most recent birthday being first. Their should also be some validation in place which doesn't allow dates over a year ago or in the future. Also if the api was down the app should be able to fail gracefully and return a simple error message.
> 
> The date should be displayed in the following format "15th January 2019"
> 
> **Expectations**
>
> Try to keep this task as simple as possible, it's suggested that you use Laravel to build this. While we know it seems a little overkill this is more to test your understanding of the framework and how you would use it. There are no frontend requirements so feel free to use standard html or base bootstrap, your choice.
> 
> Once you have completed the task please push the work to the repository provided in the email.
> 
> Resources
> 
>     http://fixer.io/
>     https://laracasts.com/series/laravel-from-scratch-2017
>     https://laravel.com/docs/5.5
>     https://appdividend.com/2017/08/20/laravel-5-5-tutorial-example/
