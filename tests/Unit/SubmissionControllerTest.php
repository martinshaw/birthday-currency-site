<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Controllers\SubmissionController;
use App\ExternalServices\FixerCurrencyService;
use App\Http\Requests\SubmissionRequest;
use App\Submission;
use Carbon\Carbon;

class SubmissionControllerTest extends TestCase
{
    public function testIndex()
    {
        $controller = app(SubmissionController::class);
        $action = $controller->index(app(FixerCurrencyService::class));
        
        $this->assertInstanceOf('Illuminate\View\View', $action);
        $this->assertTrue(strpos($action->getPath(), 'resources/views/welcome.blade.php') !== false);
    }

    public function testStoreValidSymbol()
    {
        $controller = app(SubmissionController::class);
        $request = new SubmissionRequest([
            'birthdateDay' => '01',
            'birthdateMonth' => 'January',
            'currency' => 'USD',
        ]);
        $action = $controller->store($request, app(FixerCurrencyService::class));
        
        $this->assertInstanceOf('Illuminate\Http\RedirectResponse', $action);

        $matches = [];
        $this->assertEquals(1, preg_match('/^.*\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/i', $action->getTargetUrl(), $matches));
    }
    
    public function testShow()
    {
        $controller = app(SubmissionController::class);
        $controller->index(app(FixerCurrencyService::class));
        $action = $controller->show(app(\App\Repositories\CurrenciesHistoryRepository::class), Submission::first()->uuid);
        
        $this->assertInstanceOf('Illuminate\View\View', $action);
        $this->assertTrue(strpos($action->getPath(), 'resources/views/details.blade.php') !== false);
    }

    public function testLatestBirthdayFromStoreRequest()
    {
        $controller = app(SubmissionController::class);
        $request = new SubmissionRequest([
            'birthdateDay' => '01',
            'birthdateMonth' => 'January',
            'currency' => 'USD',
        ]);
        $action = $controller->buildLatestBirthdayFromStoreRequest($request);

        $this->assertEquals(new Carbon('2020-01-01 00:00:00'), $action);
    }
}
